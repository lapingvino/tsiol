# Tsiol: a gemini/ipfs bridge

Try this bridge at gemini://gemini.kief.tk/ip[f|n]s/(hash/key/domain)(/path)

You need a local running IPFS instance to run this bridge.
