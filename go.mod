module git.kiefte.eu/lapingvino/tsiol

go 1.15

require (
	git.sr.ht/~adnano/go-gemini v0.1.13
	github.com/LukeEmmet/html2gemini v0.0.0-20201115162526-e63bbe688236
	github.com/ipfs/go-ipfs-api v0.2.0
)
